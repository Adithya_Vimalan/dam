import json
import boto3

def handler(event, context):
    # TODO implement
    if event["Records"][0]["cf"]['config']['eventType']=='viewer-request':
        request = event["Records"][0]["cf"]["request"]
        headers = request["headers"]
        print(f"Client IP : {request['clientIp']}, Request method : {request['method']}, Url : {request['uri']}")
        return request
    elif event['Records'][0]["cf"]['config']['eventType']=='viewer-response':
        response = event["Records"][0]["cf"]["response"]
        headers = response["headers"]
        headers["strict-transport-security"] = [{"key": "Strict-Transport-Security","value": "max-age=31536000; includeSubdomains; preload"}]
        headers["content-security-policy"] = [{"key": "Content-Security-Policy","value": "default-src 'none'; font-src https://fonts.gstatic.com; img-src 'self' https://www.gravatar.com; script-src 'self'; style-src 'self' 'unsafe-inline'  https://fonts.googleapis.com/"}]
        headers["x-content-type-options"] = [{"key": "X-Content-Type-Options","value": "nosniff"}]
        headers["x-frame-options"] = [{"key": "X-Frame-Options", "value": "DENY"}]
        headers["x-xss-protection"] = [{"key": "X-XSS-Protection", "value": "1; mode=block"}]
        headers["referrer-policy"] = [{"key": "Referrer-Policy", "value": "same-origin"}]
        headers["feature-policy"] = [{"key": "Feature-Policy","value": "accelerometer 'none'; ambient-light-sensor 'none'; autoplay 'none'; camera 'none'; encrypted-media 'none'; focus-without-user-activation 'none'; fullscreen 'none'; geolocation 'none'; gyroscope 'none'; magnetometer 'none'; microphone 'none'; midi 'none'; payment 'none'; picture-in-picture 'none'; speaker 'none'; sync-xhr 'none'; usb  'none'; vr 'none'"}]
        return response